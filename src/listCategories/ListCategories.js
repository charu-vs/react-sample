import React, { Component } from "react";
import ReactDOM from "react-dom";

import SampleCategories from "../constants/SampleCategories";

import "./_listCategories.scss";

export class ListCategories extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: SampleCategories,
      clearSelection: true
    };
  }

  showSubcategory = (name='', index=0) => {
    return () => {
      const categories = this.state.categories;
      this.setState({clearSelection : false})
        if (name === categories[index].name) {
            const SubCategory = categories[index].items.map(
            ({ description='', id='', name='' }, categoryIndex) => {
                return (
                <div
                    key={id}
                    onClick={this.showDescription(categories, index, categoryIndex)}
                >
                    {name}
                </div>
                );
            }
            );
            ReactDOM.render(SubCategory, document.getElementById("subcategory"));
        }
    };
  };

  showDescription = (categories={}, categoryIndex=0, index=0) => {
    return () => {
      const Description = categories[index].items[categoryIndex].description;
      ReactDOM.render(
        <div>DESCRIPTION : {Description}</div>,
        document.getElementById("description")
      );
    };
  };

  handleClearButtonToTrue = () => {
      this.setState({clearSelection:true})
  }
  handleClearButtonToFalse = () => {
    this.setState({clearSelection:false})
  }

  render() {
    const { categories={}, clearSelection=true } = this.state;
    return (
      <>
        <div className="box-category-wrapper">
          <CategoryBox
            categories={categories}
            showSubcategory={this.showSubcategory}
          />
          <button disabled={clearSelection} onClick={this.handleClearButtonToTrue}>Clear Selection</button>
        </div>
        {/* {clearSelection ? null :  */}
            <div className="box-subcategory-wrapper">
            <div id="subcategory"></div>
            <div id="description"></div>
            </div> 
        {/* } */}
      </>
    );
  }
}

const CategoryBox = ({ categories={}, showSubcategory=()=>{} }) => {
  return categories.map(({ name='' }, index) => {
    return (
      <div>
        <div
          className="box-category"
          name={name}
          onClick={showSubcategory(name, index)}
        >
          {name}
        </div>
      </div>
    );
  });
};

export default ListCategories;
