import React from 'react';

import LoginInputFields from '../constants/LoginInputField';

import './_login.scss'

const Credentials = {
	"email": "info@vectoscalar.com",
	"password": "vs123"
}

export default class Login extends React.Component {
    constructor(props){
        super(props);

        this.state = { 
            input : LoginInputFields,
            isButtonDisabled : true,
            array : [0,0]
        }
    }

    handleChange = (index='') => {
        return (event) => {
            const input = this.state.input;
            const array = this.state.array;
            if(event.target.value){
                input[index].Value = event.target.value;
                array[index] = 1;
                this.setState({input,array})
            }
            else{
                input[index].Value = event.target.value;
                array[index] = 0;
                this.setState({array})
            }
        }
    }

    handleSubmit = (event={}) =>{
        event.preventDefault();
        console.log("submit");
    }

    handleButtonDisable = (value=false) => {
        if(this.state.isButtonDisabled !== value){
            this.setState({isButtonDisabled:value})
        }
    }

    handleButtonState = (array=[]) => {
        const getValueForButton= array.reduce((acc,element)=>{
            acc = acc && element;
            return acc;
        },1)
        if(getValueForButton){
            this.handleButtonDisable(false)
        }
        else{
            this.handleButtonDisable(true)
        }
    }

    checkCredentials = () => {
        const {input} = this.state;
        let isValidCredential = false
        if(input[0].Value === Credentials.email && input[1].Value === Credentials.password){
            isValidCredential = true
        }
        return isValidCredential;
    }

    render() {
        const { input={},isButtonDisabled=true ,array=[]} = this.state;
        return (
            <div className="login-details-wrapper">
                <form onSubmit={this.handleSubmit}>
                    <InputFields input={input} onChange={this.handleChange}/>
                    {this.handleButtonState(array)}
                    <LoginButton checkCredentials={this.checkCredentials} isButtonDisabled={isButtonDisabled}>
                        <p>Invalid Login Credentials</p>
                    </LoginButton>
                </form>
            </div> 
        )
    }
    
}

const InputFields = ({ input={}, onChange=()=>{} }) => {
    return (
        input.map(({ ID='', NAME='', TYPE='', Value='' },index) => {
            return (
                <div>
                    <input key={ID} name={NAME} onChange={onChange(index)} placeholder={NAME} type={TYPE} value={Value} />
                </div>
            )
        })
    )
}

const LoginButton = ({children={}, checkCredentials=()=>{}, isButtonDisabled=true}) => {
    return (
        <div>
            <button disabled={isButtonDisabled} name="Login" type="sumbit">Login</button>
            {!isButtonDisabled && !checkCredentials() && children}
        </div>
    )
}
