import React from 'react';

import Login from './login/Login';
import ListCategories from './listCategories/ListCategories';

import './App.css';

function App() {
  return (
    <div className="App">
      {/* NOTE: uncomment components one at a time to see their functionality*/}
      <Login />
      {/* <ListCategories /> */}
    </div>
  );
}

export default App;
